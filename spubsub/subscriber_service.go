package spubsub

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	sdms "gitlab.sendo.vn/core/golang-sdk"
)

// easy interface for implement subscribers
type SubscriberService interface {
	sdms.RunnableService
}

type SubscriberConfig struct {
	Event string
	// default: max server-side allowed number
	MaxConcurrent int32
	// Max retry - if value is zero it will retry forever
	MaxRetrySubscribe int32

	ClientService PubsubClientServiceInterface
	ProcessFunc   func(*Message)
	// should wait for all ProcessFunc exit before stop app
	WaitForProcessFunc bool
}

const (
	// SleepTimeBeforeRetry is the duration should sleep before every retry
	SleepTimeBeforeRetry time.Duration = 10 * time.Second
)

func NewSubscriberService(cfg *SubscriberConfig) SubscriberService {
	if cfg.Event == "" {
		panic("SubscriberConfig.Event is not set")
	}
	if cfg.ClientService == nil {
		panic("SubscriberConfig.ClientService is not set")
	}
	if cfg.ProcessFunc == nil {
		panic("SubscriberConfig.ProcessFunc is not set")
	}
	return &subscriberService{
		cfg: *cfg,
	}
}

type subscriberService struct {
	cfg SubscriberConfig

	cancelSub context.CancelFunc

	wg sync.WaitGroup

	isStopped bool
}

func (s *subscriberService) InitFlags() {}

func (s *subscriberService) Configure() error { return nil }

func (s *subscriberService) Run() error {
	numberRetries := int32(0)
	var err error
	for {
		if s.isStopped {
			return nil
		}
		err := s.run()
		// The code should not be error
		// If error only the mgs chan is closed
		if err != nil {
			fmt.Printf("Could not subscribe to event %s, due to %s\n", s.cfg.Event, err)
		}
		numberRetries++
		if s.cfg.MaxRetrySubscribe > 0 &&
			numberRetries > s.cfg.MaxRetrySubscribe {
			break
		}
		time.Sleep(SleepTimeBeforeRetry)
	}
	return err
}

func (s *subscriberService) run() error {
	cli := s.cfg.ClientService.GetPubsubClient()

	var ctx context.Context
	ctx, s.cancelSub = context.WithCancel(context.Background())
	defer s.cancelSub()

	opt := SubscribeOption{
		Event:         s.cfg.Event,
		Token:         s.cfg.ClientService.GetSubToken(s.cfg.Event),
		MaxConcurrent: s.cfg.MaxConcurrent,
	}
	msgs, err := cli.Subscribe(ctx, &opt)

	if err != nil {
		// Just leave the error here so the parent func can decide about retry or panic
		return err
	}

	for m := range msgs {
		go s.processMessage(m)
	}

	// This should not touch here
	return errors.New("The mgs channel is close, so maybe the bug is occurred")
}

func (s *subscriberService) processMessage(m *Message) {
	s.wg.Add(1)
	defer s.wg.Done()

	s.cfg.ProcessFunc(m)

	if !m.hadAck {
		panic("ProcessFunc must call Message.Ack or Message.Redeliver!")
	}
}

func (s *subscriberService) Stop() {
	s.isStopped = true
	if s.cancelSub != nil {
		s.cancelSub()
	}
	if s.cfg.WaitForProcessFunc {
		s.wg.Wait()
	}
}

func (s *subscriberService) Cleanup() {}
