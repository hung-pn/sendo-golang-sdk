module gitlab.sendo.vn/core/golang-sdk

require (
	contrib.go.opencensus.io/exporter/jaeger v0.2.0 // indirect
	github.com/facebookgo/flagenv v0.0.0-20160425205200-fcd59fca7456
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/gogo/protobuf v1.2.1
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/mock v1.2.0 // indirect	github.com/hashicorp/consul/api v1.0.0
	github.com/hashicorp/consul/api v1.1.0
	github.com/jinzhu/gorm v1.9.8
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.1.1 // indirect
	github.com/mailru/easyjson v0.0.0-20190403194419-1ea4449da983 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.2.0
	github.com/ugorji/go v1.1.4 // indirect
	github.com/viettranx/grpc-gateway v1.5.7
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	gitlab.sendo.vn/protobuf/internal-apis-go v1.3.22
	go.opencensus.io v0.21.0
	golang.org/x/net v0.0.0-20190326090315-15845e8f865b
	google.golang.org/grpc v1.19.1
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
)
