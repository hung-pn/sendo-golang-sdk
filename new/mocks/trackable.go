// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"
import registry "gitlab.sendo.vn/core/golang-sdk/new/registry"

// Trackable is an autogenerated mock type for the Trackable type
type Trackable struct {
	mock.Mock
}

// CheckKV provides a mock function with given fields: _a0
func (_m *Trackable) CheckKV(_a0 registry.Agent) {
	_m.Called(_a0)
}

// Deregister provides a mock function with given fields: _a0
func (_m *Trackable) Deregister(_a0 registry.Agent) {
	_m.Called(_a0)
}

// Register provides a mock function with given fields: _a0
func (_m *Trackable) Register(_a0 registry.Agent) {
	_m.Called(_a0)
}
