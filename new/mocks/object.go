// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import descriptor "github.com/golang/protobuf/protoc-gen-go/descriptor"

import mock "github.com/stretchr/testify/mock"

// Object is an autogenerated mock type for the Object type
type Object struct {
	mock.Mock
}

// File provides a mock function with given fields:
func (_m *Object) File() *descriptor.FileDescriptorProto {
	ret := _m.Called()

	var r0 *descriptor.FileDescriptorProto
	if rf, ok := ret.Get(0).(func() *descriptor.FileDescriptorProto); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*descriptor.FileDescriptorProto)
		}
	}

	return r0
}

// PackageName provides a mock function with given fields:
func (_m *Object) PackageName() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// TypeName provides a mock function with given fields:
func (_m *Object) TypeName() []string {
	ret := _m.Called()

	var r0 []string
	if rf, ok := ret.Get(0).(func() []string); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	return r0
}
