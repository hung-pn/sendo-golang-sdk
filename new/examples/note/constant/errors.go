package constant

type ErrID int

const (
	ErrEmptyText ErrID = iota + 410
	ErrNoteNotFound
	ErrNoteCannotList
	ErrNoteCannotUpdate
	ErrNoteCannotDelete
	ErrNoteCannotAdd
)

func (s ErrID) String() string {
	return [...]string{
		"Text can not be blank",
		"Note is not found",
		"Can not list note",
		"Note updated failed",
		"Note deleted failed",
		"Note added failed",
	}[s-410]
}

func (s ErrID) Error() string {
	return s.String()
}
