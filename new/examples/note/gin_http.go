package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.sendo.vn/core/golang-sdk/new"
	"gitlab.sendo.vn/core/golang-sdk/new/examples/note/handlers"
	"gitlab.sendo.vn/protobuf/internal-apis-go/demo"
	"net/http"
)

const noteEndpoint = "notes"

// This is an example for REST API service,
// without gRPC, use gin library as a router engine
func router(service sendo.Service) func(router *gin.Engine) {

	return func(router *gin.Engine) {
		v1 := router.Group(fmt.Sprintf("/v1/%s", noteEndpoint))
		{
			v1.GET("", getNotesHandler(service))
			v1.POST("", addNotesHandler(service))
			// We can continue with delete and update note
		}
	}
}

// Get note handler
func getNotesHandler(service sendo.Service) func(*gin.Context) {
	return func(c *gin.Context) {
		requestData := new(demo.NoteListReq)

		if err := c.Bind(requestData); err != nil {
			c.JSON(http.StatusBadRequest, SimpleErrResponse(ErrInvalidRequest(err)))
			return
		}

		result, err := handlers.NewListHandler(getRepo(service), context.Background(), requestData).Handle()

		if err != nil {
			c.JSON(http.StatusBadRequest, SimpleErrResponse(ErrCannotFetchData(err)))
			return
		}

		c.JSON(http.StatusOK, SimpleSuccessResponse(result))
	}
}

// Create note handler
func addNotesHandler(service sendo.Service) func(*gin.Context) {
	return func(c *gin.Context) {
		requestData := new(demo.NoteAddReq)

		if err := c.Bind(requestData); err != nil {
			c.JSON(http.StatusBadRequest, SimpleErrResponse(ErrInvalidRequest(err)))
			return
		}

		result, err := handlers.NewAddHandler(getRepo(service), context.Background(), requestData).Handle()

		if err != nil {
			c.JSON(http.StatusBadRequest, SimpleErrResponse(ErrCannotFetchData(err)))
			return
		}

		c.JSON(http.StatusOK, SimpleSuccessResponse(result))
	}
}

// Error helpers
var (
	ErrCannotFetchData = func(err error) error {
		return newAppErr(err, http.StatusBadRequest, "can not fetch data")
	}
	ErrInvalidRequest = func(err error) error {
		return newAppErr(err, http.StatusBadRequest, "invalid request")
	}
)

type AppError struct {
	// We don't show root cause to the clients
	RootCause error `json:"-"`
	Code      int
	Message   string
}

func newAppErr(err error, code int, msg string) AppError {
	return AppError{RootCause: err, Code: code, Message: msg}
}

func (ae AppError) Error() string {
	return ae.Message
}

// Response helpers
var (
	SimpleSuccessResponse = func(data interface{}) Response {
		return newResponse(http.StatusOK, data, nil, nil)
	}

	SimpleErrResponse = func(data interface{}) Response {
		return newResponse(http.StatusBadRequest, data, nil, nil)
	}
)

type Response struct {
	Code  int         `json:"code"`
	Data  interface{} `json:"data"`
	Param interface{} `json:"param,omitempty"`
	Other interface{} `json:"other,omitempty"`
}

func newResponse(code int, data, param, other interface{}) Response {
	return Response{
		Code:  code,
		Data:  data,
		Param: param,
		Other: other,
	}
}
