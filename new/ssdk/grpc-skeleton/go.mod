module example

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/jinzhu/gorm v1.9.8 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/mailru/easyjson v0.0.0-20190403194419-1ea4449da983 // indirect
	github.com/olivere/elastic v6.2.17+incompatible // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/viettranx/grpc-gateway v1.5.7
	gitlab.sendo.vn/core/golang-sdk v1.2.10
	gitlab.sendo.vn/protobuf/internal-apis-go v1.3.79
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092
	google.golang.org/grpc v1.21.0
)
