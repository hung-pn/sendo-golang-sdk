package mssql

import (
	"database/sql"
	"flag"
	_ "github.com/denisenkom/go-mssqldb"
	"gitlab.sendo.vn/core/golang-sdk/new/logger"
)

type MSSQLOpt struct {
	Prefix string
	Uri    string
}

type mssql struct {
	name   string
	conn   *sql.DB
	logger logger.Logger
	*MSSQLOpt
}

func NewMSSQL(name, flagPrefix string) *mssql {
	return &mssql{
		name: name,
		//logger: logger.GetCurrent().GetLogger(name),
		MSSQLOpt: &MSSQLOpt{
			Prefix: flagPrefix,
		},
	}
}

func (mssql *mssql) GetPrefix() string {
	return mssql.Prefix
}

func (mssql *mssql) isDisabled() bool {
	return mssql.Uri == ""
}

func (mssql *mssql) InitFlags() {
	prefix := mssql.Prefix
	if mssql.Prefix != "" {
		prefix += "-"
	}

	flag.StringVar(&mssql.Uri, prefix+"mssql-uri", "", "MS SQL connection-string. Ex: sqlserver://username:password@host/instance?param1=value&param2=value")
}

func (mssql *mssql) Configure() error {
	mssql.logger = logger.GetCurrent().GetLogger(mssql.name)

	if mssql.isDisabled() {
		return nil
	}

	mssql.logger.Info("Connecting to MS SQL at ", mssql.Uri, "...")
	conn, err := sql.Open("mssql", mssql.Uri)

	if err != nil {
		mssql.logger.Error("Cannot connect to MSSQL. ", err.Error())
		return err
	}

	mssql.conn = conn
	return nil
}

func (mssql *mssql) Name() string {
	return mssql.name
}

func (mssql *mssql) Run() error {
	return mssql.Configure()
}

func (mssql *mssql) Stop() <-chan bool {
	c := make(chan bool)
	go func() {
		if mssql.conn != nil {
			_ = mssql.conn.Close()
		}
		c <- true
	}()
	return c
}

func (mssql *mssql) Get() interface{} {
	return mssql.conn
}
