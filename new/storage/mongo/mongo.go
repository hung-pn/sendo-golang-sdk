package mongo

import (
	"flag"
	"gitlab.sendo.vn/core/golang-sdk/new/logger"
	"time"

	"github.com/globalsign/mgo"
)

var (
	defaultDBName = "defaultMongoDB"
)

const retryCount = 20

type MongoDBOpt struct {
	MgoUri string
	Prefix string
}

type mongoDB struct {
	name      string
	logger    logger.Logger
	session   *mgo.Session
	isRunning bool
	*MongoDBOpt
}

func NewMongoDB(name, prefix string) *mongoDB {
	return &mongoDB{
		MongoDBOpt: &MongoDBOpt{
			Prefix: prefix,
		},
		name:      name,
		isRunning: false,
	}
}

func (mgDB *mongoDB) GetPrefix() string {
	return mgDB.Prefix
}

func (mgDB *mongoDB) Name() string {
	return mgDB.name
}

func (mgDB *mongoDB) InitFlags() {
	prefix := mgDB.Prefix
	if mgDB.Prefix != "" {
		prefix += "-"
	}

	flag.StringVar(&mgDB.MgoUri, prefix+"mgo-uri", "", "MongoDB connection-string. Ex: mongodb://...")
}

func (mgDB *mongoDB) isDisabled() bool {
	return mgDB.MgoUri == ""
}

func (mgDB *mongoDB) Configure() error {
	if mgDB.isDisabled() || mgDB.isRunning {
		return nil
	}

	mgDB.logger = logger.GetCurrent().GetLogger(mgDB.name)
	mgDB.logger.Info("Connect to Mongodb at ", mgDB.MgoUri, " ...")

	var err error
	mgDB.session, err = mgDB.getConnWithRetry(retryCount)
	if err != nil {
		mgDB.logger.Error("Error connect to mongodb at ", mgDB.MgoUri, ". ", err.Error())
		return err
	}
	mgDB.isRunning = true
	return nil
}

func (mgDB *mongoDB) Cleanup() {
	if mgDB.isDisabled() {
		return
	}

	if mgDB.session != nil {
		mgDB.session.Close()
	}
}

func (mgDB *mongoDB) Run() error {
	return mgDB.Configure()
}

func (mgDB *mongoDB) Stop() <-chan bool {
	if mgDB.session != nil {
		mgDB.session.Close()
	}
	mgDB.isRunning = false

	c := make(chan bool)
	go func() { c <- true }()
	return c
}

func (mgDB *mongoDB) Get() interface{} {
	var err error
	if mgDB.session == nil {
		mgDB.session, err = mgDB.getConnWithRetry(retryCount)
		if err != nil {
			mgDB.logger.Fatalf("%s connection cannot reconnect\n", mgDB.name)
		}
	}
	return mgDB.session
}

func (mgDB *mongoDB) getConnWithRetry(retryCount int) (*mgo.Session, error) {
	db, err := mgo.Dial(mgDB.MgoUri)
    
    if err == nil {
        return db, err
    }
	for i := 1; i <= retryCount; i++ {
		time.Sleep(time.Second * 2)
		mgDB.logger.Errorf("Retry to connect %s (%d).\n", mgDB.name, i)
		db, err = mgo.Dial(mgDB.MgoUri)
		if err == nil {
			break
		}
	}
    return db, err
}
