package redis

import (
	"flag"
	"gitlab.sendo.vn/core/golang-sdk/new/logger"
	"sync"
	"time"

	"github.com/gomodule/redigo/redis"
)

var (
	defaultMaxActive = 0 // 0 is unlimited max active connection
	defaultMaxIdle   = 10
	retryCount       = 20
)

type RedisDBOpt struct {
	RedisUri     string
	Prefix       string
	PingInterval int // in seconds
}

type redisDB struct {
	name      string
	pool      *redis.Pool
	logger    logger.Logger
	isRunning bool
	once      *sync.Once
	*RedisDBOpt
}

func NewRedisDB(name, flagPrefix string) *redisDB {

	p := &redis.Pool{
		IdleTimeout: time.Minute,
	}

	r := &redisDB{
		name: name,
		//logger: logger.GetCurrent().GetLogger(name),
		pool: p,
		RedisDBOpt: &RedisDBOpt{
			Prefix: flagPrefix,
		},
		once: new(sync.Once),
	}
	p.Dial = r.dialFunc

	return r
}

func (r *redisDB) GetPrefix() string {
	return r.Prefix
}

func (r *redisDB) isDisabled() bool {
	return r.RedisUri == ""
}

func (r *redisDB) InitFlags() {
	prefix := r.Prefix
	if r.Prefix != "" {
		prefix += "-"
	}

	flag.StringVar(&r.RedisUri, prefix+"redis-uri", "", "Redis connection-string. Ex: redis://localhost/0")
	flag.IntVar(&r.pool.MaxActive, prefix+"redis-pool-max-active", defaultMaxActive, "Override redis pool MaxActive")
	flag.IntVar(&r.pool.MaxIdle, prefix+"redis-pool-max-idle", defaultMaxIdle, "Override redis pool MaxIdle")
	flag.IntVar(&r.PingInterval, prefix+"redis-ping-interval", 5, "Redis ping check interval")
}

func (r *redisDB) Configure() error {
	r.logger = logger.GetCurrent().GetLogger(r.name)

	if r.isDisabled() {
		return nil
	}

	r.logger.Info("Connecting to Redis at ", r.RedisUri, "...")

	// just test config
	c := r.pool.Get()

	if err := c.Err(); err != nil {
		r.logger.Error("Cannot connect Redis. ", err.Error())
		return err
	}

	r.isRunning = true
	_ = c.Close()

	return nil
}

func (r *redisDB) dialFunc() (redis.Conn, error) {
	//return redis.DialURL(r.RedisUri)
	return redis.DialURL(r.RedisUri)
}

func (r *redisDB) Name() string {
	return r.name
}

func (r *redisDB) Get() interface{} {
	r.once.Do(func() {
		for i := 1; i <= retryCount; i++ {
			if !r.isRunning && !r.isDisabled() {
				if pConn := r.pool.Get(); pConn.Err() != nil {
					r.isRunning = true
					_ = pConn.Close()
					go r.reconnectIfNeeded()
					break
				} else {
					time.Sleep(time.Second * time.Duration(time.Second*3))
				}
			}
		}

		if !r.isRunning && !r.isDisabled() {
			r.logger.Fatalf("%s connection cannot reconnect\n", r.name)
		}
	})

	if r.pool == nil {
		return nil
	}
	return r.pool
}

func (r *redisDB) Run() error {
	return r.Configure()
}

func (r *redisDB) Stop() <-chan bool {
	if r.pool != nil {
		_ = r.pool.Close()
	}

	c := make(chan bool)
	go func() { c <- true }()
	return c
}

func (r *redisDB) reconnectIfNeeded() {
	p := r.pool

	for {
		if pConn := p.Get(); pConn.Err() != nil {
			_ = pConn.Close()
			r.logger.Errorf("%s connection is gone, try to reconnect\n", r.name)
			r.isRunning = false
			r.once = new(sync.Once)

			_ = r.Get()
			return
		}
		time.Sleep(time.Second * time.Duration(r.PingInterval))
	}
}
