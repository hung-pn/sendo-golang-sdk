package goredis

// Go-Redis is an alternative option for Redis
// Github: https://github.com/go-redis/redis
//
// It supports:
// 		Redis 3 commands except QUIT, MONITOR, SLOWLOG and SYNC.
// 		Automatic connection pooling with circuit breaker support.
// 		Pub/Sub.
// 		Transactions.
// 		Pipeline and TxPipeline.
// 		Scripting.
// 		Timeouts.
// 		Redis Sentinel.
// 		Redis Cluster.
// 		Cluster of Redis Servers without using cluster mode and Redis Sentinel.
// 		Ring.
// 		Instrumentation.
// 		Cache friendly.
// 		Rate limiting.
// 		Distributed Locks.

import (
	"crypto/tls"
	"errors"
	"flag"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.sendo.vn/core/golang-sdk/new/logger"
	"net"
	"net/url"
	"strconv"
	"strings"
)

var (
	defaultGoRedisMaxActive = 0 // 0 is unlimited max active connection
	defaultGoMaxIdle        = 10
)

type GoRedisDBOpt struct {
	Prefix    string
	RedisUri  string
	MaxActive int
	MaxIde    int
}

type goRedisDB struct {
	name   string
	client *redis.Client
	logger logger.Logger
	*GoRedisDBOpt
}

func NewGoRedisDB(name, flagPrefix string) *goRedisDB {
	return &goRedisDB{
		name: name,
		//logger: logger.GetCurrent().GetLogger(name),
		GoRedisDBOpt: &GoRedisDBOpt{
			Prefix:    flagPrefix,
			MaxActive: defaultGoMaxIdle,
			MaxIde:    defaultGoRedisMaxActive,
		},
	}
}

func (r *goRedisDB) GetPrefix() string {
	return r.Prefix
}

func (r *goRedisDB) isDisabled() bool {
	return r.RedisUri == ""
}

func (r *goRedisDB) InitFlags() {
	prefix := r.Prefix
	if r.Prefix != "" {
		prefix += "-"
	}

	flag.StringVar(&r.RedisUri, prefix+"go-redis-uri", "", "(For go-redis) Redis connection-string. Ex: redis://localhost/0")
	flag.IntVar(&r.MaxActive, prefix+"go-redis-pool-max-active", defaultGoRedisMaxActive, "(For go-redis) Override redis pool MaxActive")
	flag.IntVar(&r.MaxIde, prefix+"go-redis-pool-max-idle", defaultGoMaxIdle, "(For go-redis) Override redis pool MaxIdle")
}

func (r *goRedisDB) Configure() error {
	r.logger = logger.GetCurrent().GetLogger(r.name)

	if r.isDisabled() {
		return nil
	}

	r.logger.Info("Connecting to Redis at ", r.RedisUri, "...")

	options, err := r.parseUri()
	if err != nil {
		r.logger.Error("parse RedisUri error. ", err.Error())
		return err
	}
	client := redis.NewClient(&options)

	//// Ping to test Redis connection
	//if err := client.Ping().Err(); err != nil {
	//	r.logger.Error("Cannot connect Redis. ", err.Error())
	//	return err
	//}

	// Connect successfully, assign client to goRedisDB
	r.logger.Info(fmt.Sprintf("Connect %sredis at %s success...", r.Prefix, r.RedisUri))
	r.client = client
	return nil
}

func (r *goRedisDB) Name() string {
	return r.name
}

func (r *goRedisDB) Get() interface{} {
	return r.client
}

func (r *goRedisDB) Run() error {
	return r.Configure()
}

func (r *goRedisDB) Stop() <-chan bool {
	if r.client != nil {
		if err := r.client.Close(); err != nil {
			r.logger.Info("cannot close ", r.name)
		}
	}

	c := make(chan bool)
	go func() { c <- true }()
	return c
}

func (r *goRedisDB) parseUri() (redis.Options, error) {
	var o redis.Options
	u, err := url.Parse(r.RedisUri)
	if err != nil {
		return o, err
	}

	if u.Scheme != "redis" && u.Scheme != "rediss" {
		return o, errors.New("invalid redis URL scheme: " + u.Scheme)
	}

	if u.User != nil {
		if p, ok := u.User.Password(); ok {
			o.Password = p
		}
	}

	if len(u.Query()) > 0 {
		return o, errors.New("no options supported")
	}

	h, p, err := net.SplitHostPort(u.Host)
	if err != nil {
		h = u.Host
	}
	if h == "" {
		h = "localhost"
	}
	if p == "" {
		p = "6379"
	}
	o.Addr = net.JoinHostPort(h, p)

	f := strings.FieldsFunc(u.Path, func(r rune) bool {
		return r == '/'
	})
	switch len(f) {
	case 0:
		o.DB = 0
	case 1:
		if o.DB, err = strconv.Atoi(f[0]); err != nil {
			return o, fmt.Errorf("invalid redis database number: %q", f[0])
		}
	default:
		return o, errors.New("invalid redis URL path: " + u.Path)
	}

	if u.Scheme == "rediss" {
		o.TLSConfig = &tls.Config{ServerName: h}
	}
	o.PoolSize = r.MaxActive
	o.MinIdleConns = r.MaxIde

	return o, nil
}
