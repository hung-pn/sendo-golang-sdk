package jaeger

import (
	"flag"

	jg "contrib.go.opencensus.io/exporter/jaeger"
	"gitlab.sendo.vn/core/golang-sdk/new/logger"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/trace"
)

type jaeger struct {
	logger            logger.Logger
	stopChan          chan bool
	processName       string
	sampleTraceRating float64
	agentURI          string
	stdTracingEnabled bool
}

func NewJaeger(processName string) *jaeger {
	return &jaeger{
		processName: processName,
		stopChan:    make(chan bool),
	}
}

func (j *jaeger) Name() string {
	return "jaeger"
}

func (j *jaeger) InitFlags() {
	flag.Float64Var(
		&j.sampleTraceRating,
		"jaeger-trace-sample-rate",
		1.0,
		"sample rating for remote tracing from OpenSensus: 0.0 -> 1.0 (default is 1.0)",
	)

	flag.StringVar(
		&j.agentURI,
		"jaeger-agent-uri",
		"",
		"jaeger agent URI to receive tracing data directly",
	)

	flag.BoolVar(
		&j.stdTracingEnabled,
		"jaeger-std-enabled",
		false,
		"enable tracing export to std (default is false)",
	)
}

func (j *jaeger) Configure() error {
	j.logger = logger.GetCurrent().GetLogger(j.Name())
	return nil
}

func (j *jaeger) Run() error {
	if err := j.Configure(); err != nil {
		return err
	}
	return j.connectToJaegerAgent()
}

func (j *jaeger) Stop() <-chan bool {
	go func() {
		if !j.isEnabled() {
			j.stopChan <- true
			return
		}

		j.stopChan <- true
	}()

	return j.stopChan
}

func (j *jaeger) isEnabled() bool {
	return j.agentURI != ""
}

func (j *jaeger) connectToJaegerAgent() error {
	if !j.isEnabled() {
		return nil
	}

	j.logger.Infof("connecting to Jaeger Agent on %s...", j.agentURI)

	je, err := jg.NewExporter(jg.Options{
		AgentEndpoint: j.agentURI,
		Process:       jg.Process{ServiceName: j.processName},
	})

	if err != nil {
		return err
	}

	// And now finally register it as a Trace Exporter
	trace.RegisterExporter(je)

	// Trace view for console
	if j.stdTracingEnabled {
		// Register stats and trace exporters to export
		// the collected data.
		view.RegisterExporter(&PrintExporter{})

		// Register the views to collect server request count.
		if err := view.Register(ocgrpc.DefaultServerViews...); err != nil {
			j.logger.Errorf("jaeger error: %s", err.Error())
		}
	}

	if j.sampleTraceRating >= 1 {
		trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
	} else {
		trace.ApplyConfig(trace.Config{DefaultSampler: trace.ProbabilitySampler(j.sampleTraceRating)})
	}

	return nil
}
