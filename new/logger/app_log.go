package logger

import (
	"flag"
	"strings"

	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

var (
	DEFAULT_LOGGER_SERVICE = NewAppLogService(&LoggerConfig{
		FlagPrefix:   "core-",
		BasePrefix:   "core",
		DefaultLevel: "info",
	})
	haveInitGlobalFlag bool
	includeSource      bool
)

type ExcludePrefixFunc func(string) bool

type LoggerConfig struct {
	// prefix to flag
	FlagPrefix string

	DefaultLevel string
	// log prefix
	BasePrefix string

	ExcludePrefix ExcludePrefixFunc
}

type LoggerService interface {
	GetLogger(prefix string) Logger
	SetLevel(level string) error
}

// A default app logger
//
// Just write everything to console
type appLogServiceImpl struct {
	logger *logrus.Logger
	cfg    LoggerConfig

	// flags
	logLevel string
}

func NewAppLogService(config *LoggerConfig) LoggerService {
	if config == nil {
		config = &LoggerConfig{}
	}

	if config.DefaultLevel == "" {
		config.DefaultLevel = "info"
	}

	logger := logrus.New()
	logger.Formatter = logrus.Formatter(&prefixed.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "15:04:05",
	})
	lv := mustParseLevel(config.DefaultLevel)
	logger.SetLevel(lv)

	return &appLogServiceImpl{
		logger:   logger,
		cfg:      *config,
		logLevel: config.DefaultLevel,
	}
}

func (s *appLogServiceImpl) InitFlags() {
	flag.StringVar(&s.logLevel, s.cfg.FlagPrefix+"loglevel", "debug", "Log level: panic | fatal | error | warn | info | debug | trace")

	if !haveInitGlobalFlag {
		flag.BoolVar(&includeSource, "log-source", false, "Include source line number in log message")
		haveInitGlobalFlag = true
	}
}

func (s *appLogServiceImpl) Configure() error {
	lv := mustParseLevel(s.logLevel)
	s.logger.SetLevel(lv)

	return nil
}

func (s *appLogServiceImpl) Cleanup() {
}

func (s *appLogServiceImpl) GetLogger(prefix string) Logger {
	var entry *logrus.Entry

	prefix = s.cfg.BasePrefix + "." + prefix
	prefix = strings.Trim(prefix, ".")
	if prefix == "" {
		entry = logrus.NewEntry(s.logger)
	} else {
		entry = s.logger.WithField("prefix", prefix)
	}

	l := &logger{entry}
	var log Logger = l

	if includeSource {
		log = &loggerWithSrc{l}
	}

	if s.cfg.ExcludePrefix != nil && s.cfg.ExcludePrefix(prefix) {
		log = &nullLogger{log}
	}

	return log
}

func (s *appLogServiceImpl) SetLevel(lvl string) error {
	lv, err := logrus.ParseLevel(lvl)
	if err != nil {
		return err
	}
	s.logLevel = lvl
	s.logger.SetLevel(lv)
	return nil
}
